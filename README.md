## News & New Tech ##

1. [1st web designer](http://www.1stwebdesigner.com/ "1st web designer")
2. [Little Big Details](http://littlebigdetails.com/ "Little Big Details")
3. [One Extra Pixel](http://www.onextrapixel.com/ "One Extra Pixel")
4. [Six Revisions](http://sixrevisions.com/ "Six Revision")
5. [Speckyboy Web Design Magazine](http://speckyboy.com/)
6. [WordPress News At WPMU](http://premium.wpmudev.org/blog)
7. [Smashing Magazine Feed](http://www.smashingmagazine.com/)
8. [Gaaks](http://www.gaaks.com/)
9. [Elemis](http://elemisfreebies.com/)


## Design & Free PSD ##

1. [Blog Spoon Graphic](http://blog.spoongraphics.co.uk/ "Blog Spoon Graphic")
2. [Photoshop Plus](http://www.photoshop-plus.co.uk/)
3. [Psdchest](http://www.psdchest.com/)
4. [PSDCORE - Photoshop Tutorials](http://www.psdcore.com/)
5. [Blaz Robar.Com](http://www.blazrobar.com/)
6. [PixelsDaily](http://blog.creativevip.net/)
7. [FREE PSD FILES](http://freepsdfiles.net/)



## Inspiration & Idea ##

1. [Design Bump](http://designbump.com/ "Design Bump")
2. [Design Instruct](http://designinstruct.com/ "Design Instruct")
3. [Design Woop](http://designwoop.com/ "Design Woop")
4. [Line 25](http://line25.com/ "Line 25")
5. [Mag Press](http://www.magpress.com/ "Mag Press")
6. [Web Design Blog](http://webdesignfan.com/)
7. [Web Design Ledger](http://webdesignledger.com/)
8. [Web Designer Wall](http://webdesignerwall.com/)
9. [Webdesigner Depot](http://www.webdesignerdepot.com/)


## Tutorials & Goodies ##

1. [Codrops](http://tympanus.net/codrops "Codrops")
2. [CSS Tricks](https://css-tricks.com/ "CSS Tricks")
3. [Daily Js](http://dailyjs.com/ "Daily Js")
4. [David Walsh Blog](http://davidwalsh.name/ "David Walsh Blog")
5. [Paulund](http://www.paulund.co.uk/ "Paulund")
6. [Tutorialzine](http://tutorialzine.com/)
7. [WordPress For Beginners](http://www.wpbeginner.com/)
8. [WpRecipes.Com](http://www.wprecipes.com/)
9. [Wptuts+](http://code.tutsplus.com/)
10. [CatsWhoCode.Com](http://www.catswhocode.com/blog)
11. [Script Tutorials](http://www.script-tutorials.com/)


## WP Framework ##

**Free**

1. [Gantry](http://gantry-framework.org/)

**Premium**

1. [Genesis](http://my.studiopress.com/themes/genesis/)
2. [Thesis](http://diythemes.com/)
3. [WooFramework + Canvas Theme](http://www.woothemes.com/products/canvas/)